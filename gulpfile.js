/* ==========================================================================
   Gulp - Builder manager for the Front-end
   ========================================================================== */

/* Configuration variables
   ========================================================================== */

/* Set dependencies required for the tasks */
const gulp = require('gulp'),
  concat = require('gulp-concat'),
  filter = require('gulp-filter'),
  rename = require('gulp-rename'),
  gulpMain = require('./modules/gulp-main.js'),
  clean = require('gulp-clean'),
  minify = require('gulp-minify'),
  cleanCSS = require('gulp-clean-css'),
  { series, parallel } = require('gulp'),
  pug = require('gulp-pug'),
  assignGTM = require('./modules/assign-gtm')

/* Landing page destination */

const distFolder = 'dist/'
const destinationVendors = `${distFolder}/vendors/`
const landingOrigin = 'vendors/'

/* Set the filters */
const jsFilter = filter('**/*.js'),
  cssFilter = filter('**/*.css'),
  /* we need to filter out MD fonts as it will have its own filter */
  fontFilter = filter(['**/*.{otf,eot,svg,ttf,woff,woff2}', '!**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}'], { restore: true }),
  mdIconsFilter = filter('**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}')


/* Extract files for landing vendors.
  ========================================================================== */
/* Clean the built folder to start a fresh building */
const cleanVendors = () => {
  return gulp.src(destinationVendors, { read: false, allowEmpty: true })
    .pipe(clean())
}

/* Extract all css files declared in the mainfiles object */
const extractLandingVendorsCss = () => {
  return gulp.src(gulpMain(landingOrigin), { allowEmpty: true })
    .pipe(cssFilter)
    .pipe(concat('vendors.css'))
    .pipe(cleanCSS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(`${destinationVendors}css`))
}

/* Extract all js files declared in the mainfiles object */
const extractLandingVendorsJs = () => {
  return gulp.src(gulpMain(landingOrigin), { allowEmpty: true })
    .pipe(jsFilter)
    .pipe(concat('vendors.js'))
    .pipe(minify({
      ext: {
        min: '.min.js'
      }
    }))
    .pipe(gulp.dest(`${destinationVendors}js`))
}

/* Extract all font files declared in the mainfiles object */
const extractLandingFonts = () => {
  return gulp.src(gulpMain(landingOrigin), { allowEmpty: true })
    .pipe(fontFilter)
    .pipe(gulp.dest(`${destinationVendors}fonts`)) // move all fonts, except for MD icons to the /fonts folder as per fontawesome and eos-icons default configuration
    .pipe(fontFilter.restore)
    .pipe(mdIconsFilter)
    .pipe(gulp.dest(`${destinationVendors}css`)) // Material icons .css file is configured to have the css and fonts in the same folder
}

/* Converte pug to HTML */
const pugToHtml = () => {
  return gulp.src('./views/landing-page/*.pug')
    .pipe(pug({
      doctype: 'html',
      pretty: false
    }))
    .pipe(gulp.dest(`${distFolder}`));
}

/* Move assets from assetts to static folder */
const moveMultimedia = () => {
  return gulp.src('./assets/images/**/*')
    .pipe(gulp.dest(`${distFolder}assets/images/`));
}

const movePublic = () => {
  return gulp.src('./resources/**/*')
    .pipe(gulp.dest(`${distFolder}`));
}

const setGTM = (done) => {
  assignGTM('./dist/index.html', '${GTM_ENV_VARIABLE}', process.env.EOS_GTM_ID)
    .then(response => {
      if (response.status === 'ok') return done()

      /* We log some others messages that are NOT status === 'ok' and we exit the process*/
      console.log(response)
      process.exit()
    })
}

/* Configure the default gulp task and one for the landing page
   ========================================================================== */

// Tasks exported for individual use
exports.cleanVendors = cleanVendors
exports.extractLandingVendorsCss = extractLandingVendorsCss
exports.extractLandingVendorsJs = extractLandingVendorsJs
exports.extractLandingFonts = extractLandingFonts
exports.pugToHtml = pugToHtml
exports.moveMultimedia = moveMultimedia
exports.movePublic = movePublic
exports.setGTM = setGTM

exports.buildVendors =
  series(cleanVendors,
    parallel(
      extractLandingVendorsCss,
      extractLandingVendorsJs,
      extractLandingFonts
    ))
