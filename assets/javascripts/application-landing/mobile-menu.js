/* Menu state based on scroll position, if device menu is open and screensize. */
const state = {
  isTop: true,
  isOpen: false,
  isSmall: null
}

$(function () {
  /* DOM selectors */
  const mainMenu = $('.js-main-menu')
  const mobileMenu = $('.js-mobile-submenu-landing')
  const burgerMenu = $('.js-landing-mobile-burger')

  /* Trigger the burger menus when is clicked */
  burgerMenu.click(() => {
    openSubmenu({ state, mobileMenu, mainMenu })
    addBackgroundColor({ state, mainMenu })
  })

  /* Functionality based on scroll */
  $(window).bind('scroll', function () {
    /* Set the isTop state based on scroll position */
    $(window).scrollTop() <= 100
      ? state.isTop = true
      : state.isTop = false

    /* Check if it should add or remove the bg */
    return addBackgroundColor({ state, mainMenu })
  })

  /* Functioanlity based on windows size */
  window.onresize = () => {
    window.innerWidth > 769
      ? state.isSmall = false
      : state.isSmall = true

    enableSubmenuSmallDevice({ state, mobileMenu })
    addBackgroundColor({ state, mainMenu })

    state.isOpen = false
  }

  /* Initial render windows size */
  window.innerWidth > 769
    ? state.isSmall = false
    : state.isSmall = true

  enableSubmenuSmallDevice({ state, mobileMenu, mainMenu })
})

const addBackgroundColor = params => {
  const { state: { isTop, isOpen, isSmall }, mainMenu } = params

  if (isTop && isOpen) {
    mainMenu.addClass('solid-bg')
  } else if (isTop && !isOpen) {
    mainMenu.removeClass('solid-bg')
  } else if (isTop && !isSmall) {
    mainMenu.removeClass('solid-bg')
  } else {
    mainMenu.addClass('solid-bg')
  }
}

const openSubmenu = params => {
  const { mobileMenu, mainMenu } = params

  mobileMenu.slideToggle(100)
  mobileMenu.toggleClass('isOpen')
  mobileMenu.css('display', 'flex')
  mainMenu.addClass('solid-bg')

  state.isOpen ? state.isOpen = false : state.isOpen = true
}

const enableSubmenuSmallDevice = params => {
  const { state: { isSmall }, mobileMenu } = params

  !isSmall
    ? mobileMenu.css('display', 'block')
    : mobileMenu.slideUp()
}
