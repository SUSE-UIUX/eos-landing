const fs = require('fs');

module.exports = async (filePath, toReplace, replaceWith) => {
  try {
    let fileContent = fs.readFileSync(filePath, 'utf-8')

    if (fileContent.includes(toReplace)) {
      fs.writeFileSync(filePath, fileContent.replace(toReplace, replaceWith), 'utf-8')

      return {
        status: 'ok',
        msg: 'Task completed'
      }
    } else {
      return {
        status: 'error',
        msg: `${toReplace} was not found in ${filePath}`
      }
    }
  } catch (error) {
    return {
      status: 'error',
      msg: error
    }
  }
}
